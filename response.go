package response

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"runtime"

	"github.com/zenazn/goji/web"
)

/****************************************************************
** Response
********/

type Error struct {
	Domain string `json:"domain"`
	Module string `json:"module,omitempty"`
	Type   string `json:"type"`
	Desc   string `json:"desc,omitempty"`
	Target string `json:"target,omitempty"`
	Trace  string `json:"trace,omitempty"`
}

type ResponseMeta struct {
	Errors []Error `json:"errors"`
	Warns  []Error `json:"warns"`
}

type ResponseData struct {
	Payload interface{}  `json:"payload"`
	Meta    ResponseMeta `json:"meta"`
}

func (this *ResponseData) WriteJSON(w io.Writer) error {
	data, err := json.MarshalIndent(this, "", "  ")
	if err != nil {
		return err
	}
	_, err = w.Write(data)
	if err != nil {
		return err
	}
	return nil
}

type Response interface {
	http.ResponseWriter

	Error(e Error)
	Warn(e Error)
	Send(payload interface{}) error
	Payload(payload interface{})
	HaveErrors() bool
}

var _ Response = new(response)

type ResponseOptions struct {
	DefDomain string
	Debug     bool
}

type response struct {
	w    http.ResponseWriter
	data ResponseData
	ops  ResponseOptions

	raw, sent bool
	inactive  bool
}

func NewResponse(w http.ResponseWriter, ops ResponseOptions) Response {
	r := &response{
		w:   w,
		ops: ops,
	}

	return r
}

func (this *response) Error(e Error) {
	if e.Domain == "" {
		if this.ops.DefDomain != "" {
			e.Domain = this.ops.DefDomain
		} else {
			e.Domain = "generic"
		}
	}
	this.data.Meta.Errors = append(this.data.Meta.Errors, e)
	return
}

func (this *response) Warn(e Error) {
	if e.Domain == "" && this.ops.DefDomain != "" {
		e.Domain = this.ops.DefDomain
	}
	this.data.Meta.Warns = append(this.data.Meta.Warns, e)
	return
}

func (this *response) Send(payload interface{}) error {
	if this.inactive {
		return nil
	}

	if payload != nil {
		this.data.Payload = payload
	}

	this.w.Header().Set("Content-Type", "application/json; encoding=utf-8")
	this.w.WriteHeader(http.StatusOK)

	err := this.data.WriteJSON(this.w)
	if err != nil {
		return err
	}

	this.sent = true

	return nil
}

func (this *response) Payload(payload interface{}) {
	this.data.Payload = payload

	return
}

func (this *response) HaveErrors() bool {
	return len(this.data.Meta.Errors) > 0
}

func (this *response) Defer() {
	if this.inactive {
		return
	}

	discardPayloadAndSend := func() {
		this.data.Payload = nil
		this.Error(Error{
			Type: "payload_discarded",
		})
		err := this.Send(nil)
		if err != nil {
			panic(err)
		}
	}

	if rec := recover(); rec != nil {
		if !this.raw && !this.sent {
			ise := Error{
				Type: "ise",
			}
			if this.ops.Debug {
				ise.Desc = fmt.Sprint(rec)
				var trace [1024 * 8 * 8]byte
				ise.Trace = string(trace[0:runtime.Stack(trace[:], false)])
				log.Println(ise.Desc)
				log.Println(ise.Trace)
			}
			this.Error(ise)
			err := this.Send(nil)
			if err != nil {
				discardPayloadAndSend()
			}
		}
	} else {
		if !this.raw && !this.sent {
			err := this.Send(nil)
			if err != nil {
				discardPayloadAndSend()
			}
		}
	}
	return
}

func (this *response) Header() http.Header {
	return this.w.Header()
}

func (this *response) Write(data []byte) (int, error) {
	this.raw = true
	this.sent = true
	return this.w.Write(data)
}

func (this *response) WriteHeader(status int) {
	this.raw = true
	this.sent = true
	this.w.WriteHeader(status)
}

/****************************************************************
** Goji middleware
********/

const reqCtxKey = "response.Response"

func C(c interface{}) Response {
	switch c := c.(type) {
	case *web.C:
		if c.Env != nil {
			return c.Env[reqCtxKey].(Response)
		}
	case web.C:
		if c.Env != nil {
			return c.Env[reqCtxKey].(Response)
		}
	}
	return nil
}

func ResponseGoji(ops ResponseOptions) func(c *web.C, h http.Handler) http.Handler {

	return func(c *web.C, h http.Handler) http.Handler {

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			{ //> Replace previous Response in middleware chain
				respi, ok := w.(Response)
				if ok {
					resp := (respi.(*response))
					w = resp.w
					resp.inactive = true
				}
			}

			resp := NewResponse(w, ops)
			defer (resp.(*response)).Defer()

			if c.Env != nil {
				c.Env[reqCtxKey] = resp
			}

			h.ServeHTTP(resp, r)
		})
	}
}
